#!/bin/sh

rm -r bin/*
chmod +x Build.cmd
./Build.cmd

mkdir ../.package
cp -R src ../.package/src
cp -R bin ../.package/bin
mkdir ../.package/doc
cp doc/*.pdf ../.package/doc/
cp Build.cmd ../.package/Build.cmd
cp Run.cmd ../.package/Run.cmd

rm ../upgsp.zip
cd ../.package
zip -r ../upgsp.zip *
cd ..
rm -r .package
