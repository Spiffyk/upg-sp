package ostava.upgsp;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import ostava.upgsp.core.DistanceChartData;
import ostava.upgsp.core.ShootingCalculator;
import ostava.upgsp.core.World;
import ostava.upgsp.core.WorldRenderer;
import ostava.upgsp.core.ShootingCalculator.Status;
import ostava.upgsp.core.ShotVisualiser;
import ostava.upgsp.entities.Shooter;
import ostava.upgsp.entities.Target;
import ostava.upgsp.entities.Terrain;
import ostava.upgsp.util.CLIMessenger;
import ostava.upgsp.util.DialogMessenger;
import ostava.upgsp.util.Messenger;
import ostava.upgsp.util.TerrainFileHandler;

/**
 * The main game class
 * 
 * @author Oto Stava A16B0148P
 */
public class Game {

	public static final String APP_NAME = "Prototyp 3";
	public static final String AUTHOR = "O. Stava A16B0148P";
	public static final String TITLE = APP_NAME + " / " + AUTHOR;
	
	public static final double ELEVATION_STEP = 0.5;

	private static boolean guiMode;

	private static Messenger messenger;
	private static ShootingCalculator sc;
	private static JFrame frame;
	private static World world;
	private static WorldRenderer worldRenderer;
	private static ShotVisualiser shotVisualiser;

	private static void printHelp() {
		System.out.println(TITLE + "\n----------\n"
				+ "Strili se zadanim tri realnych cisel oddelenych mezerou ve tvaru \"<azimut> <elevace> <pocatecni rychlost>\".\n"
				+ "Trojice cisel se vzdy zadavaji po radcich.\n"
				+ "Korektni ukonceni programu se provede zavrenim vykreslovaciho okna.\n"
				+ "Po zadani trojice cisel se program zepta, zda chce hrac strilet. Pokud nechce, provede se vizualizece.\n");
	}

	private static void printWorldInfo() {
		final Target target = world.getTarget();
		final Shooter shooter = world.getShooter();
		final Terrain terrain = world.getTerrain();

		System.out.println("Smer vetru: " + String.format("%.1f", Math.toDegrees(world.getWindAzimuth())) + "°");
		System.out.println("Rychlost vetru: " + String.format("%.3f", world.getWindVelocity()) + " m/s");
		System.out.println("Nadm. vyska strelce: "
				+ String.format("%.3f", terrain.getAltitude(shooter.getX(), shooter.getY())) + " m");
		System.out.println(
				"Nadm. vyska cile: " + String.format("%.3f", terrain.getAltitude(target.getX(), target.getY())) + " m");
		System.out.println("Vzdalenost strelce od cile: " + String.format("%.3f", shooter.distanceFrom(target)) + " m");
		System.out.println();
	}

	private static JFrame createFrame() {
		final JFrame frame = new JFrame(TITLE);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		worldRenderer = new WorldRenderer(world);
		frame.add(worldRenderer, BorderLayout.CENTER);
		shotVisualiser = new ShotVisualiser(world);
		frame.add(shotVisualiser, BorderLayout.PAGE_START);
		shotVisualiser.setVisible(false);
		if (guiMode) {
			JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));

			panel.add(new JLabel("Azimut"));
			final JTextField azimuthField = new JTextField(6);
			azimuthField.getDocument().addDocumentListener(new AzimuthListener(azimuthField));
			panel.add(azimuthField);

			panel.add(new JLabel("Elevace"));
			final JTextField elevationField = new JTextField(6);
			panel.add(elevationField);

			panel.add(new JLabel("Poc. rychlost"));
			final JTextField initialVelocityField = new JTextField(6);
			panel.add(initialVelocityField);

			final JButton submitButton = new JButton("PAL!");
			submitButton.addActionListener(new ShootListener(azimuthField, elevationField, initialVelocityField));
			panel.add(submitButton);
			frame.getRootPane().setDefaultButton(submitButton);

			final JButton simulateButton = new JButton("Simuluj.");
			simulateButton.addActionListener(new SimulateListener(azimuthField, elevationField, initialVelocityField));
			panel.add(simulateButton);

			frame.add(panel, BorderLayout.PAGE_END);
		}
		frame.pack();
		frame.setMinimumSize(new Dimension(frame.getWidth(), 450));
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		return frame;
	}

	private static File fileDialog() {
		JFileChooser chooser = new JFileChooser(".");
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Terrain files (*.ter)", "ter");
		chooser.setDialogTitle("Vyberte .ter soubor");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			return chooser.getSelectedFile();
		} else {
			System.out.println("Nebyl vybran zadny soubor.");
			System.exit(0);
			return null;
		}
	}
	
	/**
	 * Validates elevation and initial velocity values. If invalid, shows a message informing about the fact.
	 * @param elevation Must be between -90 and 90 (inclusive).
	 * @param initialVelocity Must be more than 0
	 * @return {@code true} if values are valid, otherwise {@code false}.
	 */
	private static boolean validateInputValues(double elevation, double initialVelocity) {
		if (elevation >= -90 && elevation <= 90) {
			if (initialVelocity > 0) {
				return true;
			} else {
				messenger.error("Pocatecni rychlost musi byt nezaporna");
				return false;
			}
		} else {
			messenger.error("Elevace musi byt mezi -90 a 90");
			return false;
		}
	}

	private static void processGame(double azimuth, double elevation, double initialVelocity) {
		if (validateInputValues(elevation, initialVelocity)) {
			shotVisualiser.setVisible(false);
			sc.shoot(azimuth, elevation, initialVelocity);

			final Status status = sc.getStatus();

			if (status != Status.OUT_OF_BOUNDS) {
				sc.applyDamage();
			}

			frame.repaint();
			switch (status) {
			case HIT:
				messenger.info("Zasah!", "Zasah");
				break;
			case MISS:
				messenger.error("Vedle!", "Vedle");
				break;
			case OUT_OF_BOUNDS:
				messenger.error("Mimo mapu!", "Vedle");
				break;
			case NOT_SHOT:
				messenger.error("Nedoslo k vystrelu...", "Bez vystrelu???");
				break;
			}

			world.update();
			frame.repaint();
		}
	}
	
	private static void processVisualisation(double azimuth, double elevation, double initialVelocity) {
		if (validateInputValues(elevation, initialVelocity)) {
			shotVisualiser.setVisible(true);
			// Visualise elevations
			final List<Double> distances = new ArrayList<>();
			final double minElev = elevation / 2;
			final double maxElev = Math.min(90, elevation * 2);
			double topDistance = 0;
			
			for (double simElev = minElev; simElev <= maxElev; simElev += ELEVATION_STEP) {
				sc.shoot(azimuth, simElev, initialVelocity, false, false);
				final double distance = sc.getHitspot().distanceFrom(world.getShooter());
				distances.add(distance);
				if (distance > topDistance) {
					topDistance = distance;
				}
			}
			
			final DistanceChartData distanceChartData = new DistanceChartData();
			distanceChartData.setDistances(distances);
			distanceChartData.setMinElevation(minElev);
			distanceChartData.setMaxElevation(maxElev);
			distanceChartData.setTopDistance(topDistance);
			shotVisualiser.setDistanceChartData(distanceChartData);
			
			// Visualise side view
			sc.shoot(azimuth, elevation, initialVelocity);
			frame.repaint();
//			world.clearHitspot();
		}
	}

	private static void playInCLI() {
		final BufferedReader r = new BufferedReader(new InputStreamReader(System.in));

		do {
			try {
				System.out.print("Zadejte azimut, elevaci a pocatecni rychlost: ");
				String line = r.readLine().trim();

				String[] input = line.split(" ");
				double azimuth = Double.parseDouble(input[0]);
				double elevation = Double.parseDouble(input[1]);
				double initialVelocity = Double.parseDouble(input[2]);
				
				System.out.print("Chcete strilet? (Y/n): ");
				
				String optionLine = r.readLine().trim().toLowerCase();
				if (optionLine.startsWith("n")) {
					processVisualisation(azimuth, elevation, initialVelocity);
				} else {
					processGame(azimuth, elevation, initialVelocity);
				}
			} catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
				messenger.error("Chybny format zadani");
			} catch (IOException e) {
				System.err.println("Chyba vstupu.");
				e.printStackTrace();
				System.exit(10);
			}
		} while (true);
	}

	/**
	 * The entry point of the program
	 * 
	 * @param args
	 *            Last argument sets the file to load
	 */
	public static void main(String[] args) {
		printHelp();

		File file;

		if (args.length > 0) {
			guiMode = false;
			messenger = new CLIMessenger();
			file = new File(args[args.length - 1]);
		} else {
			guiMode = true;
			messenger = new DialogMessenger();
			file = fileDialog();
		}

		world = null;

		try {
			world = TerrainFileHandler.load(file, messenger);
		} catch (FileNotFoundException e) {
			messenger.error("File not found");
			System.exit(1);
		}

		world.initWind();
		printWorldInfo();
		frame = createFrame();
		sc = new ShootingCalculator(world);

		if (guiMode) {
			((DialogMessenger) messenger).setParent(frame);
		} else {
			frame.repaint();
			playInCLI();
		}
	}

	private static abstract class GameActionListener implements ActionListener {
		private JTextField azimuthField;
		private JTextField elevationField;
		private JTextField initialVelocityField;

		public GameActionListener(JTextField azimuthField, JTextField elevationField, JTextField initialVelocityField) {
			this.azimuthField = azimuthField;
			this.elevationField = elevationField;
			this.initialVelocityField = initialVelocityField;
		}

		@Override
		public void actionPerformed(ActionEvent event) {
			double azimuth, elevation, initialVelocity;

			try {
				azimuth = Double.parseDouble(azimuthField.getText());
				elevation = Double.parseDouble(elevationField.getText());
				initialVelocity = Double.parseDouble(initialVelocityField.getText());

				doAction(azimuth, elevation, initialVelocity);
			} catch (NumberFormatException e) {
				messenger.error("Chybny format zadani");
			}
		}
		
		public abstract void doAction(double azimuth, double elevation, double initialVelocity);
	}
	
	private static class ShootListener extends GameActionListener {

		public ShootListener(JTextField azimuthField, JTextField elevationField, JTextField initialVelocityField) {
			super(azimuthField, elevationField, initialVelocityField);
		}
		
		@Override
		public void doAction(double azimuth, double elevation, double initialVelocity) {
			processGame(azimuth, elevation, initialVelocity);
		}
	}
	
	private static class SimulateListener extends GameActionListener {

		public SimulateListener(JTextField azimuthField, JTextField elevationField, JTextField initialVelocityField) {
			super(azimuthField, elevationField, initialVelocityField);
		}

		@Override
		public void doAction(double azimuth, double elevation, double initialVelocity) {
			processVisualisation(azimuth, elevation, initialVelocity);
		}
	}
	
	private static class AzimuthListener implements DocumentListener {
		private JTextField azimuthField;
		
		public AzimuthListener(JTextField azimuthField) {
			this.azimuthField = azimuthField;
		}
		
		@Override
		public void insertUpdate(DocumentEvent e) {
			update();
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			update();
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			update();
		}
		
		private void update() {
			final String s = azimuthField.getText();
			worldRenderer.setShowProtractor(!s.isEmpty());
			try {
				double azimuth = Double.parseDouble(s);
				worldRenderer.setProtractorAngle(Math.toRadians(360 - azimuth));
			} catch (NumberFormatException e) {
				worldRenderer.setProtractorAngle(Double.NaN);
			}
		}
	}
}
