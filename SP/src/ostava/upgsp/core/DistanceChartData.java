package ostava.upgsp.core;

import java.util.List;

public class DistanceChartData {
	private double minElevation;
	private double maxElevation;
	private double topDistance;
	private List<Double> distances;
	
	/**
	 * @return the minElevation
	 */
	public double getMinElevation() {
		return minElevation;
	}
	/**
	 * @param minElevation the minElevation to set
	 */
	public void setMinElevation(double minElevation) {
		this.minElevation = minElevation;
	}
	/**
	 * @return the maxElevation
	 */
	public double getMaxElevation() {
		return maxElevation;
	}
	/**
	 * @param maxElevation the maxElevation to set
	 */
	public void setMaxElevation(double maxElevation) {
		this.maxElevation = maxElevation;
	}
	
	public double getTopDistance() {
		return topDistance;
	}
	
	public void setTopDistance(double topDistance) {
		this.topDistance = topDistance;
	}
	/**
	 * @return the distances
	 */
	public List<Double> getDistances() {
		return distances;
	}
	/**
	 * @param distances the distances to set
	 */
	public void setDistances(List<Double> distances) {
		this.distances = distances;
	}
	
	
}
