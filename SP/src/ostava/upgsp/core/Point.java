package ostava.upgsp.core;

/**
 * A 2D point
 * 
 * @author Oto Stava A16B0148P
 */
public abstract class Point {
	/**
	 * X-coordinate of the point
	 */
	protected double x;
	
	/**
	 * Y-coordinate of the point
	 */
	protected double y;
	
	/**
	 * The world the point is placed in
	 */
	protected World world;
	
	public Point(double x, double y, World world) {
		this.x = x;
		this.y = y;
		this.world = world;
	}
	
	public Point(double x, double y) {
		this(x, y, null);
	}
	
	/**
	 * Gets the X-coordinate of the instance in metres
	 * @return the X
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * Gets the Y-coordinate of the instance in metres
	 * @return the Y
	 */
	public double getY() {
		return y;
	}
	
	public void setWorld(World world) {
		this.world = world;
	}
	
	public World getWorld() {
		return this.world;
	}
	
	/**
	 * Gets the square of the distance from {@code o}
	 * @param o The other object
	 * @return The distance
	 */
	public final double squaredDistanceFrom(Point o)  {
		return squaredDistanceOf(this.getX(), this.getY(), o.getX(), o.getY());
	}
	
	/**
	 * Gets the distance from {@code o}
	 * @param o The other object
	 * @return The distance
	 */
	public final double distanceFrom(Point o) {
		return distanceOf(this.getX(), this.getY(), o.getX(), o.getY());
	}
	
	public static final double squaredDistanceOf(double x1, double y1, double x2, double y2) {
		final double x = x1 - x2;
		final double y = y1 - y2;
		return x*x + y*y;
	}
	
	public static final double distanceOf(double x1, double y1, double x2, double y2) {
		return Math.sqrt(squaredDistanceOf(x1, y1, x2, y2));
	}
}
