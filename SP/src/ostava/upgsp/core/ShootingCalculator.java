package ostava.upgsp.core;

import java.util.ArrayList;

import ostava.upgsp.entities.Hitspot;

public class ShootingCalculator {
	
	/**
	 * Gravitational acceleration in m&nbsp;s<sup>-2</sup>
	 */
	private static final double GRAVITY = 10;
	
	/**
	 * Wind effect coefficient in s<sup>-1</sup>
	 */
	private static final double WIND_EFFECT = 0.05;
	
	/**
	 * Time step in seconds
	 */
	private static final double TIME_STEP = 0.01;
	
	/**
	 * The depth of the crater created by a hit in m
	 */
	private static final double DAMAGE_DEPTH = 8.0;
	
	private final World world;
	
	private Hitspot hitspot;
	
	/**
	 * Creates a new shooting calculator with the specified shooter and target
	 * @param world the world
	 */
	public ShootingCalculator(World world) {
		this.world = world;
	}
	
	/**
	 * Shoots
	 * @param azimuth Azimuth in degrees
	 * @param distance Distance
	 */
	public void shoot(double azimuth, double distance) {
		final double radAzimuth = (azimuth / 360) * Math.PI * 2;
		final double x = world.shooter.getX() + (Math.cos(radAzimuth) * distance);
		final double y = world.shooter.getY() - (Math.sin(radAzimuth) * distance);
		this.hitspot = new Hitspot(x, y);
		world.setHitspot(this.hitspot);
	}
	
	/**
	 * Shoots
	 * @param azimuth Azimuth in degrees
	 * @param elevation Elevation in degrees
	 * @param initialVelocity Initial velocity in m/s
	 */
	public void shoot(double azimuth, double elevation, double initialVelocity) {
		shoot(azimuth, elevation, initialVelocity, true, true);
	}
	
	/**
	 * Shoots or simulates a shot
	 * @param azimuth Azimuth in degrees
	 * @param elevation Elevation in degrees
	 * @param initialVelocity Initial velocity in m/s
	 * @param applyWind Whether wind should be applied
	 * @param applyTerrain Whether terrain should be applied
	 */
	public void shoot(double azimuth, double elevation, double initialVelocity, boolean applyWind, boolean applyTerrain) {
		final double radAzimuth = (azimuth / 360) * Math.PI * 2;
		final double radElevation = (elevation / 360) * Math.PI * 2;
		final double windAzimuth = world.getWindAzimuth();
		final double windVelocity = world.getWindVelocity();
		
		final ArrayList<double[]> trajectory = new ArrayList<>();
		
		double wvx;
		double wvy;
		
		if (applyWind) {
			wvx = Math.cos(windAzimuth) * windVelocity;
			wvy = Math.sin(windAzimuth) * windVelocity;
		} else {
			wvx = 0;
			wvy = 0;
		}
		
		double posx = world.shooter.getX();
		double posy = world.shooter.getY();
		Double posz = (applyTerrain) ? world.terrain.getAltitude(posx, posy) : new Double(0.0);
		double lastx = posx;
		double lasty = posy;
		
		if (posz == null) {
			throw new IllegalStateException("The starting position was outside of the map");
		}
		
		trajectory.add(new double[] {posx, posy, posz, 0.0});
		
		double xyMultiplier = Math.cos(radElevation) * initialVelocity;
		double velx = Math.cos(radAzimuth) *  xyMultiplier;
		double vely = - Math.sin(radAzimuth) * xyMultiplier;
		double velz = Math.sin(radElevation) * initialVelocity;
		
		while(true) {
			posx += velx * TIME_STEP;
			posy += vely * TIME_STEP;
			posz += velz * TIME_STEP;
			
			final double distance = Point.distanceOf(posx, posy, lastx, lasty);
			trajectory.add(new double[] {posx, posy, posz, distance});
			lastx = posx;
			lasty = posy;
			
			final Double alt = (applyTerrain) ? world.terrain.getAltitude(posx, posy) : new Double(0.0);
			if (alt == null) {
				this.hitspot = new Hitspot(posx, posy, trajectory.toArray(new double[trajectory.size()][]), false);
				break;
			} else if (posz < alt) {
				this.hitspot = new Hitspot(posx, posy, trajectory.toArray(new double[trajectory.size()][]));
				break;
			}
			
			velx += (wvx - velx) * WIND_EFFECT * TIME_STEP;
			vely += (wvy - vely) * WIND_EFFECT * TIME_STEP;
			velz -= velx * WIND_EFFECT * TIME_STEP + GRAVITY * TIME_STEP;
		}
		
		world.setHitspot(this.hitspot);
	}
	
	/**
	 * Applies damage to the terrain.
	 * @throws IllegalStateException if no hitspot exists.
	 */
	public void applyDamage() {
		if (hitspot == null) {
			throw new IllegalStateException("A hitspot must exist in order to apply terrain damage");
		}
		
		world.getTerrain().damage(this.hitspot.x, this.hitspot.y, DAMAGE_DEPTH, Hitspot.RADIUS * 1.5);
	}
	
	/**
	 * Gets the last {@code Hitspot} created by this calculator (either in a simulation or in a shot)
	 * @return The last created {@code Hitspot}
	 */
	public Hitspot getHitspot() {
		return this.hitspot;
	}
	
	/**
	 * Checks whether the shot has hit the target
	 * @return
	 */
	@Deprecated
	public boolean hasHit() {
		return (this.hitspot.squaredDistanceFrom(world.target) < (Hitspot.RADIUS * Hitspot.RADIUS));
	}
	
	public Status getStatus() {
		if (this.hitspot == null)
			return Status.NOT_SHOT;
		
		if (!this.hitspot.hasLanded())
			return Status.OUT_OF_BOUNDS;
		
		return hasHit() ? Status.HIT : Status.MISS;
	}
	
	public static enum Status {
		NOT_SHOT, HIT, MISS, OUT_OF_BOUNDS
	}
}
