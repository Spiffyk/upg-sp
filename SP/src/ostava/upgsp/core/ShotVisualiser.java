package ostava.upgsp.core;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;

import javax.swing.JPanel;

import ostava.upgsp.entities.Hitspot;
import ostava.upgsp.entities.Terrain;

public class ShotVisualiser extends JPanel {

	private static final long serialVersionUID = 1947598686173823346L;
	
	private static final int OVERALL_MARGIN = 8;
	private static final int GRAPH_MARGIN = 38;
	
	private static final Font TITLES_FONT = new Font("Arial", Font.PLAIN, 14);
	private static final Font POINTS_FONT = new Font("Arial", Font.PLAIN, 11);
	
	private static final String DISTANCE_CHART_TITLE = "Zavislost vzdalenosti na elevaci";
	private static final String TERRAIN_SIDE_VIEW_TITLE = "Teren pod letici strelou";
	
	private static final int NUMBER_OF_ELEVATION_POINTS = 6;
	private static final int NUMBER_OF_DISTANCE_POINTS = 5;
	
	private static final int ELEV_POINTS_DIVISOR = NUMBER_OF_ELEVATION_POINTS - 1;
	private static final int DIST_POINTS_DIVISOR = NUMBER_OF_DISTANCE_POINTS - 1;
	
	private World world;
	
	private DistanceChartData distanceChartData;
	
	public ShotVisualiser(final World world) {
		super();
		this.world = world;
		updateSize();
	}
	
	private void updateSize() {
		int width = 200;
		if (this.getParent() != null) {
			width = this.getParent().getWidth();
		}
		this.setSize(width, 200);
		this.setPreferredSize(this.getSize());
	}
	
	public void setDistanceChartData(DistanceChartData distanceChartData) {
		this.distanceChartData = distanceChartData;
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		updateSize();
		int width = this.getWidth() - (2 * OVERALL_MARGIN);
		int height = this.getHeight() - (2 * OVERALL_MARGIN);
		final Graphics2D g2 = (Graphics2D) g;
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
		g2.translate(OVERALL_MARGIN, OVERALL_MARGIN);
		AffineTransform transform = g2.getTransform();
		drawDistanceChart(width / 2, height, g2);
		g2.setTransform(transform);
		
		g2.translate(width / 2, 0);
		drawTerrainSideView(width / 2, height, g2);
		g2.setTransform(transform);
	}
	
	private void drawDistanceChart(int width, int height, Graphics2D g2) {
		AffineTransform transform = g2.getTransform();
		final int graphWidth = width - 2 * GRAPH_MARGIN;
		final int graphHeight = height - 2 * GRAPH_MARGIN;
		g2.translate(GRAPH_MARGIN, graphHeight + GRAPH_MARGIN);
		g2.scale(1, -1);
		
		// Draw graph
		if (distanceChartData != null && !distanceChartData.getDistances().isEmpty()) {
			final int noOfDistances = distanceChartData.getDistances().size();
			final double widthMultiplier = (double) graphWidth / noOfDistances;
			final double heightMultiplier = graphHeight / distanceChartData.getTopDistance();
			
			final Path2D path = new Path2D.Double();
			path.moveTo(0, distanceChartData.getDistances().get(0) * heightMultiplier);
			
			for (int i = 1; i < noOfDistances; i++) {
				path.lineTo(i * widthMultiplier, distanceChartData.getDistances().get(i) * heightMultiplier);
			}
			
			g2.setColor(Color.RED);
			g2.draw(path);
		}
		
		// Draw lines
		g2.setColor(Color.BLACK);
		g2.draw(new Line2D.Double(0, 0, 0, graphHeight));
		g2.draw(new Line2D.Double(0, 0, graphWidth, 0));
		
		// Draw title
		g2.setTransform(transform);
		g2.setFont(TITLES_FONT);
		g2.setColor(Color.BLACK);
		{
			FontMetrics titleMetrics = g2.getFontMetrics();
			final int strWidth = titleMetrics.stringWidth(DISTANCE_CHART_TITLE);
			g2.drawString(DISTANCE_CHART_TITLE, width / 2 - strWidth / 2, height);
		}
		
		// Prepare for drawing points
		g2.translate(GRAPH_MARGIN, graphHeight + GRAPH_MARGIN);
		g2.setFont(POINTS_FONT);
		FontMetrics pointsMetrics = g2.getFontMetrics();
		final int ascent = pointsMetrics.getAscent();
		
		// Draw elevations
		{
			final int xStep = graphWidth / ELEV_POINTS_DIVISOR;
			final double minElevation = distanceChartData.getMinElevation();
			final double maxElevation = distanceChartData.getMaxElevation();
			final double elevStep = (maxElevation - minElevation) / ELEV_POINTS_DIVISOR;
			for (int i = 0; i <= ELEV_POINTS_DIVISOR; i++) {
				g2.drawString(((int) (minElevation + (i * elevStep))) + "°", i * xStep, ascent);
			}
		}
		
		// Draw distances
		{
			final int yStep = graphHeight / DIST_POINTS_DIVISOR;
			final double distStep = distanceChartData.getTopDistance() / DIST_POINTS_DIVISOR;
			for (int i = 0; i <= DIST_POINTS_DIVISOR; i++) {
				final String str = ((int) (i * distStep)) + " m";
				final int strWidth = pointsMetrics.stringWidth(str);
				g2.drawString(str, - strWidth, - (i * yStep));
			}
		}
	}
	
	private void drawTerrainSideView(int width, int height, Graphics2D g2) {
		AffineTransform transform = g2.getTransform();
		final int graphWidth = width - 2 * GRAPH_MARGIN;
		final int graphHeight = height - 2 * GRAPH_MARGIN;
		g2.translate(GRAPH_MARGIN, graphHeight + GRAPH_MARGIN);
		g2.scale(1, -1);
		
		final Hitspot hitspot = world.getHitspot();
		final Terrain terrain = world.getTerrain();
		if (hitspot != null) {
			final int noOfPoints = hitspot.getTrajectory().length;
			final double widthMultiplier = (double) graphWidth / hitspot.getTotalDistance();
			double position = 0.0;
			final double heightMultiplier = graphHeight / hitspot.getHighestAltitude();
			
			final double[] firstPoint = hitspot.getTrajectory()[0];
			final Path2D traj = new Path2D.Double();
			traj.moveTo(0, firstPoint[2] * heightMultiplier);
			
			final Path2D terr = new Path2D.Double();
			terr.moveTo(0, terrain.getAltitude(firstPoint[0], firstPoint[1]) * heightMultiplier);
			
			for (int i = 1; i < noOfPoints; i++) {
				final double[] point = hitspot.getTrajectory()[i];
				
				position += point[3] * widthMultiplier;
				traj.lineTo(position, point[2] * heightMultiplier);
				terr.lineTo(position, terrain.getAltitude(point[0], point[1]) * heightMultiplier);
			}
			terr.lineTo(graphWidth, 0);
			terr.lineTo(0, 0);
			terr.closePath();
			
			g2.setColor(Color.RED);
			g2.draw(traj);
			g2.setColor(Color.BLACK);
			g2.fill(terr);
		}
		
		// Draw title
		g2.setTransform(transform);
		g2.setFont(TITLES_FONT);
		g2.setColor(Color.BLACK);
		FontMetrics titleMetrics = g2.getFontMetrics();
		int strWidth = titleMetrics.stringWidth(TERRAIN_SIDE_VIEW_TITLE);
		g2.drawString(TERRAIN_SIDE_VIEW_TITLE, width / 2 - strWidth / 2, height);
	}
}
