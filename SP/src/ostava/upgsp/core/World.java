package ostava.upgsp.core;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.Random;

import ostava.upgsp.entities.Hitspot;
import ostava.upgsp.entities.Shooter;
import ostava.upgsp.entities.Target;
import ostava.upgsp.entities.Terrain;

/**
 * The game's world manager class.
 * 
 * @author Oto Stava A16B0148P
 */
public class World {
	
	/**
	 * Maximum velocity the wind can have
	 */
	public static final double MAX_WIND_VELOCITY = 17;
	
	/**
	 * Maximum delta value of the wind velocity change
	 */
	public static final double MAX_WIND_VELOCITY_CHANGE = 1;
	
	/**
	 * Maximum delta value of the wind azimuth change
	 */
	public static final double MAX_WIND_AZIMUTH_CHANGE = 0.6;
	
	/**
	 * {@code 2*PI}
	 */
	public static final double TAU = 2*Math.PI;
	
	private final Random r;
	
	/**
	 * Wind azimuth in radians
	 */
	private double windAzimuth;
	
	/**
	 * Wind velocity in m/s
	 */
	private double windVelocity;
	
	/**
	 * World's terrain
	 */
	Terrain terrain;
	
	/**
	 * Shooter
	 */
	Shooter shooter;
	
	/**
	 * Shooter's target
	 */
	Target target;
	
	/**
	 * Hitspot
	 */
	Hitspot hitspot;
	
	public World() {
		r = new Random();
		initWind();
	}
	
	

	/**
	 * Draws all entities contained in the world
	 * @param g2 Graphics context
	 * @param r The world renderer
	 */
	public void drawAll(Graphics2D g2, WorldRenderer r) {
		final AffineTransform inputTransform = g2.getTransform();
		if (terrain != null) {
			terrain.draw(g2, r);
			g2.setTransform(inputTransform);
		}
		
		if (hitspot != null) {
			hitspot.draw(g2, r);
			g2.setTransform(inputTransform);
		}
		
		if (shooter != null) {
			shooter.draw(g2, r);
			g2.setTransform(inputTransform);
		}
		
		if (target != null) {
			target.draw(g2, r);
			g2.setTransform(inputTransform);
		}
	}
	
	
	
	/**
	 * Generates random wind parameters
	 */
	public void initWind() {
		windVelocity = r.nextDouble() * MAX_WIND_VELOCITY;
		windAzimuth = r.nextDouble() * TAU;
	}
	
	/**
	 * Randomly changes wind parameters within constant boundaries
	 */
	public void changeWind() {
		setWindVelocity(windVelocity + (r.nextDouble() * MAX_WIND_VELOCITY_CHANGE * 2) - MAX_WIND_VELOCITY_CHANGE);
		setWindAzimuth(windAzimuth + (r.nextDouble() * MAX_WIND_AZIMUTH_CHANGE * 2) - MAX_WIND_AZIMUTH_CHANGE);
	}
	
	
	
	/**
	 * @return the wind azimuth in radians
	 */
	public double getWindAzimuth() {
		return windAzimuth;
	}

	/**
	 * @param windAzimuth the wind azimuth to set in radians
	 */
	public void setWindAzimuth(double windAzimuth) {
		while (windAzimuth >= TAU) {
			windAzimuth -= TAU;
		}
		
		while (windAzimuth < 0) {
			windAzimuth += TAU;
		}
		
		this.windAzimuth = windAzimuth;
	}

	/**
	 * @return the wind velocity in m/s
	 */
	public double getWindVelocity() {
		return windVelocity;
	}

	/**
	 * @param windVelocity the wind velocity to set in m/s
	 */
	public void setWindVelocity(double windVelocity) {
		if (windVelocity < 0) {
			// reverse wind azimuth
			setWindAzimuth(windAzimuth - Math.PI);
		}
		
		this.windVelocity = Math.min(MAX_WIND_VELOCITY, Math.abs(windVelocity));
	}
	
	public void update() {
		changeWind();
		this.target.update();
	}

	/**
	 * @return the terrain
	 */
	public Terrain getTerrain() {
		return terrain;
	}

	/**
	 * @param terrain the terrain to set
	 */
	public void setTerrain(Terrain terrain) {
		this.terrain = terrain;
	}

	/**
	 * @return the shooter
	 */
	public Shooter getShooter() {
		return shooter;
	}

	/**
	 * @param shooter the shooter to set
	 */
	public void setShooter(Shooter shooter) {
		shooter.setWorld(this);
		this.shooter = shooter;
	}

	/**
	 * @return the target
	 */
	public Target getTarget() {
		return target;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(Target target) {
		target.setWorld(this);
		this.target = target;
	}

	/**
	 * @return the hitspot
	 */
	public Hitspot getHitspot() {
		return hitspot;
	}

	/**
	 * @param hitspot the hitspot to set
	 */
	public void setHitspot(Hitspot hitspot) {
		hitspot.setWorld(this);
		this.hitspot = hitspot;
	}
	
	/**
	 * Convenience method to set the hitspot to {@code null}.
	 */
	public void clearHitspot() {
		setHitspot(null);
	}
	
}
