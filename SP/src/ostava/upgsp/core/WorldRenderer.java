package ostava.upgsp.core;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;

import javax.swing.JPanel;

import ostava.upgsp.entities.Terrain;

/**
 * A {@link JPanel} subclass for rendering the {@link World}.
 * 
 * @author Oto Stava A16B0148P
 */
public class WorldRenderer extends JPanel {
	/**
	 * something for serialization or whatever...
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The margin of the inner elements in the window
	 */
	private static final int MARGIN = 10;
	
	/**
	 * The font the terrain's dimensions are drawn with
	 */
	private static final Font DIMENSIONS_FONT = new Font("Arial", Font.BOLD, 11);
	
	/**
	 * The margin for the terrain dimensions in pixels
	 */
	private static final int DIMENSIONS_MARGIN = 3;
	
	private static final double WIND_ARROW_MARGIN = 3;
	private static final double WIND_ARROW_PADDING = 4;
	private static final double WIND_ARROW_MAX_LENGTH = 40;
	private static final double WIND_ARROW_OUT = 8;
	private static final double WIND_ARROW_BACK = 8;
	
	private static final double WIND_ARROW_RADIUS = WIND_ARROW_MAX_LENGTH + WIND_ARROW_PADDING;
	private static final double WIND_ARROW_DIAMETER = 2 * WIND_ARROW_RADIUS;
	
	private static final double PROTRACTOR_RADIUS = 30;
	private static final double PROTRACTOR_DIAMETER = 2 * PROTRACTOR_RADIUS;
	
	
	/**
	 * Protractor circle
	 */
	private static final Ellipse2D PROTRACTOR_CIRCLE = new Ellipse2D.Double(- PROTRACTOR_RADIUS, - PROTRACTOR_RADIUS, PROTRACTOR_DIAMETER, PROTRACTOR_DIAMETER);
	
	/**
	 * Wind indicator arrow
	 */
	private static final Path2D WIND_ARROW = new Path2D.Double();
	
	/**
	 * Wind indicator circle
	 */
	private static final Ellipse2D WIND_ARROW_CIRCLE = new Ellipse2D.Double(- WIND_ARROW_RADIUS, - WIND_ARROW_RADIUS, WIND_ARROW_DIAMETER, WIND_ARROW_DIAMETER);
	
	static {
		WIND_ARROW.append(new Line2D.Double(0, 0, - WIND_ARROW_BACK, - WIND_ARROW_OUT), false); // top part
		WIND_ARROW.append(new Line2D.Double(0, 0, - WIND_ARROW_BACK, WIND_ARROW_OUT), false); // bottom part
	}
	
	/**
	 * The world renderer
	 */
	final World world;
	
	/**
	 * The scale of pixels
	 */
	private double scale = 1.0;
	
	/**
	 * Whether the protractor should be shown
	 */
	private boolean showProtractor = false;
	
	private double protractorAngle = 0.0;
	
	/**
	 * Creates a new renderer
	 * @param world
	 */
	public WorldRenderer(World world) {
		super();
		this.setSize(400, 400);
		this.setPreferredSize(this.getSize());
		this.world = world;
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		
		final int innerWidth = Math.max(0, this.getWidth() - 2 * MARGIN);
		final int innerHeight = Math.max(0, this.getHeight() - 2 * MARGIN);
		
		final Terrain terrain = world.getTerrain();
		
		if (terrain != null) {
			scale = innerWidth / terrain.getWidth();
			if ((terrain.getHeight() * scale) > innerHeight) {
				scale = innerHeight / terrain.getHeight();
			}
		} else {
			scale = 1.0;
		}
		
		final int tWidth = (int) (terrain.getWidth() * scale);
		final int tHeight = (int) (terrain.getHeight() * scale);
		
		g2.translate(this.getWidth() / 2 - tWidth / 2, this.getHeight() / 2 - tHeight / 2);
		
		world.drawAll(g2, this);
		paintDimensions(g2, terrain);
		paintProtractor(g2);
		paintWindIndicator(g2);
	}
	
	private void paintDimensions(Graphics2D g2, Terrain terrain) {
		String terrainWidthString = terrain.getWidth() + " m";
		String terrainHeightString = terrain.getHeight() + " m";
		
		g2.setColor(Color.GREEN);
		g2.setFont(DIMENSIONS_FONT);
		FontMetrics fontMetrics = g2.getFontMetrics();
		int terrainWidthStringWidth = fontMetrics.stringWidth(terrainWidthString);
		int terrainHeightStringWidth = fontMetrics.stringWidth(terrainHeightString);
		int fontAscent = fontMetrics.getAscent();
		
		AffineTransform t = g2.getTransform();
		g2.translate(terrain.getWidth() * scale - terrainWidthStringWidth - DIMENSIONS_MARGIN, fontAscent + DIMENSIONS_MARGIN);
		g2.drawString(terrainWidthString, 0, 0);
		
		g2.setTransform(t);
		g2.rotate(Math.toRadians(90));
		g2.translate(terrain.getHeight() * scale - terrainHeightStringWidth - DIMENSIONS_MARGIN, - DIMENSIONS_MARGIN);
		g2.drawString(terrainHeightString, 0, 0);
		
		g2.setTransform(t); // cleanup
	}
	
	private void paintWindIndicator(Graphics2D g2) {
		g2.setColor(Color.GREEN);
		
		final double multiplier = world.getWindVelocity() / World.MAX_WIND_VELOCITY;
		
		AffineTransform t = g2.getTransform();
		g2.translate(WIND_ARROW_RADIUS + WIND_ARROW_MARGIN, WIND_ARROW_RADIUS + WIND_ARROW_MARGIN);
		g2.draw(WIND_ARROW_CIRCLE);
		
		g2.rotate(world.getWindAzimuth());
		g2.draw(new Line2D.Double(0, 0, WIND_ARROW_MAX_LENGTH * multiplier, 0));
		
		g2.translate(WIND_ARROW_MAX_LENGTH * multiplier, 0);
		g2.draw(WIND_ARROW);
		
		g2.setTransform(t); // cleanup
	}
	
	private void paintProtractor(Graphics2D g2) {
		AffineTransform t = g2.getTransform();
		if (showProtractor) {
			g2.setColor(Color.MAGENTA);
			g2.translate(world.getShooter().x * scale, world.getShooter().y * scale);
			g2.draw(PROTRACTOR_CIRCLE);
			if (protractorAngle != Double.NaN) {
				g2.draw(new Line2D.Double(0, 0,
						Math.cos(protractorAngle) * PROTRACTOR_RADIUS, Math.sin(protractorAngle) * PROTRACTOR_RADIUS));
			}
		}
		g2.setTransform(t); // cleanup
	}
	
	public void setShowProtractor(boolean showProtractor) {
		this.showProtractor = showProtractor;
		this.repaint();
	}
	
	/**
	 * Gets the scale of the renderer.
	 * @return The scale
	 */
	public double getScale() {
		return scale;
	}

	/**
	 * @return the protractorAngle
	 */
	public double getProtractorAngle() {
		return protractorAngle;
	}

	/**
	 * @param protractorAngle the protractorAngle to set
	 */
	public void setProtractorAngle(double protractorAngle) {
		this.protractorAngle = protractorAngle;
	}
}
