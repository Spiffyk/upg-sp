package ostava.upgsp.drawables;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import ostava.upgsp.core.Point;
import ostava.upgsp.core.WorldRenderer;

/**
 * A drawable cross of two lines.
 * 
 * @author Oto Stava A16B0148P
 */
public class Cross extends Point implements Drawable {
	
	/**
	 * The radius of the cross
	 */
	private final int radius;
	
	/**
	 * The color of the cross
	 */
	final Color color;
	
	/**
	 * Creates a new cross
	 * @param x X-coordinate of the center
	 * @param y Y-coordinate of the center
	 * @param color Color of the cross
	 * @param radius Radius of the cross
	 */
	public Cross(double x, double y, Color color, int radius) {
		super(x, y);
		this.color = color;
		this.radius = radius;
	}
	
	@Override
	public void draw(Graphics2D g2, WorldRenderer r) {
		final double scale = r.getScale();
		
		Line2D hl = new Line2D.Double(this.x * scale - radius, this.y * scale, this.x * scale + radius, this.y * scale);
		Line2D vl = new Line2D.Double(this.x * scale, this.y * scale - radius, this.x * scale, this.y * scale + radius);
		
		g2.setColor(this.color);
		g2.draw(hl);
		g2.draw(vl);
	}
}
