package ostava.upgsp.drawables;

import java.awt.Graphics2D;

import ostava.upgsp.core.WorldRenderer;

/**
 * Indicates that this object can draw itself onto a 2D graphics context.
 * 
 * @author Oto Stava A16B0148P
 */
public interface Drawable {
	
	/**
	 * Draws the entity onto the drawing context.
	 * @param g2 The 2D drawing context
	 * @param r Renderer instance
	 */
	public void draw(Graphics2D g2, WorldRenderer r);
	
}
