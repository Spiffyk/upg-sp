package ostava.upgsp.drawables;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import ostava.upgsp.core.Point;
import ostava.upgsp.core.WorldRenderer;

public class ScaledCircle extends Point implements Drawable {
	
	/**
	 * Color of the circle
	 */
	private final Color color;
	
	/**
	 * Radius in m
	 */
	private final double radius;
	
	public ScaledCircle(double x, double y, Color color, double radius) {
		super(x, y);
		this.color = color;
		this.radius = radius;
	}

	@Override
	public void draw(Graphics2D g2, WorldRenderer r) {
		final double scale = r.getScale();
		final Ellipse2D circle = new Ellipse2D.Double(
				(this.x - radius) * scale,
				(this.y - radius) * scale,
				2 * radius * scale,
				2 * radius * scale);
		
		g2.setColor(this.color);
		g2.fill(circle);
	}

}
