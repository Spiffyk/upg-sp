package ostava.upgsp.drawables;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Path2D;

import ostava.upgsp.core.Point;
import ostava.upgsp.core.WorldRenderer;

/**
 * A simple abstract class for representing objects by paths
 * 
 * @author Oto Stava A16B0148P
 */
public abstract class SimpleGraphic extends Point implements Drawable {
	
	private final Color color;
	
	private final Path2D path;
	
	/**
	 * Creates a new {@code SimpleGraphic} at the given coordinates with the given color
	 * @param x The X-coordinate
	 * @param y The Y-coordinate
	 * @param color The color to draw the graphic in
	 */
	public SimpleGraphic(double x, double y, Color color) {
		super(x, y);
		this.color = color;
		this.path = new Path2D.Double();
		createGraphic(path);
	}

	@Override
	public void draw(Graphics2D g2, WorldRenderer r) {
		g2.translate(this.x * r.getScale(), this.y * r.getScale());
		g2.setColor(color);
		g2.draw(path);
	}

	/**
	 * Appends shapes in the path to create the graphic.
	 * @param path The path to append shapes into
	 */
	protected abstract void createGraphic(Path2D path);
}
