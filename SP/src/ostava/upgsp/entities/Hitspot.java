package ostava.upgsp.entities;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;

import ostava.upgsp.core.WorldRenderer;
import ostava.upgsp.drawables.ScaledCircle;

public class Hitspot extends ScaledCircle {
	
	public static final double DIAMETER = 60;
	public static final double RADIUS = DIAMETER / 2;
	
	private static final Color MIN_ALTITUDE_COLOR = Color.RED;
	private static final Color MAX_ALTITUDE_COLOR = Color.YELLOW;
	
	/**
	 * The points representing the trajectory of the shot<br />
	 * Index 0 represents X-coordinate<br />
	 * Index 1 represents Y-coordinate<br />
	 * Index 2 represents Z-coordinate<br />
	 * Index 3 represents distance
	 */
	private final double[][] trajectory;
	
	/**
	 * The total distance the shot has gone
	 */
	private final double totalDistance;
	
	/**
	 * The lowest altitude the shot has been
	 */
	private final double lowestAltitude;
	
	/**
	 * The highest altitude the shot has been
	 */
	private final double highestAltitude;
	
	private final boolean hasLanded;

	public Hitspot(double x, double y, double[][] trajectory, boolean hasLanded) {
		super(x, y, Color.ORANGE, RADIUS);
		this.trajectory = trajectory;
		this.hasLanded = hasLanded;
		
		if (trajectory != null) {
			double lowestAltitude = Double.MAX_VALUE;
			double highestAltitude = - Double.MAX_VALUE;
			double totalDistance = 0.0;
			for (int i = 0; i < trajectory.length; i++) {
				double alt = trajectory[i][2];
				if (alt < lowestAltitude) {
					lowestAltitude = alt;
				}
				
				if (alt > highestAltitude) {
					highestAltitude = alt;
				}
				
				totalDistance += trajectory[i][3];
			}
			this.totalDistance = totalDistance;
			this.lowestAltitude = lowestAltitude;
			this.highestAltitude = highestAltitude;
		} else {
			this.totalDistance = 0.0;
			this.lowestAltitude = Double.NEGATIVE_INFINITY;
			this.highestAltitude = Double.POSITIVE_INFINITY;
		}
	}
	
	public Hitspot(double x, double y, double[][] trajectory) {
		this(x, y, trajectory, true);
	}
	
	public Hitspot(double x, double y) {
		this(x, y, null);
	}
	
	@Override
	public void draw(Graphics2D g2, WorldRenderer r) {
		if (hasLanded) {
			super.draw(g2, r);
		}
		
		final double scale = r.getScale();
//		g2.setColor(Color.RED);
		if (trajectory != null) {
			for (int i = 1; i < trajectory.length; i++) {
				double alt = trajectory[i-1][2];
				double multiplier = (alt - lowestAltitude) / (highestAltitude - lowestAltitude);
				
				int deltar = MAX_ALTITUDE_COLOR.getRed() - MIN_ALTITUDE_COLOR.getRed(),
					deltag = MAX_ALTITUDE_COLOR.getGreen() - MIN_ALTITUDE_COLOR.getGreen(),
					deltab = MAX_ALTITUDE_COLOR.getBlue() - MIN_ALTITUDE_COLOR.getBlue();
				
				int cr = (int) (MIN_ALTITUDE_COLOR.getRed() + deltar * multiplier),
					cg = (int) (MIN_ALTITUDE_COLOR.getGreen() + deltag * multiplier),
					cb = (int) (MIN_ALTITUDE_COLOR.getBlue() + deltab * multiplier);
				
				g2.setColor(new Color(cr, cg, cb));
				
				g2.draw(new Line2D.Double(
						trajectory[i-1][0] * scale,
						trajectory[i-1][1] * scale,
						trajectory[i][0] * scale,
						trajectory[i][1] * scale));
			}
		}
	}
	
	/**
	 * @return the trajectory
	 */
	public double[][] getTrajectory() {
		return trajectory;
	}

	/**
	 * @return the lowestAltitude
	 */
	public double getLowestAltitude() {
		return lowestAltitude;
	}

	/**
	 * @return the highestAltitude
	 */
	public double getHighestAltitude() {
		return highestAltitude;
	}

	/**
	 * @return the totalDistance
	 */
	public double getTotalDistance() {
		return totalDistance;
	}

	public boolean hasLanded() {
		return hasLanded;
	}
}
