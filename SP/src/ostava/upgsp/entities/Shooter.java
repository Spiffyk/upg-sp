package ostava.upgsp.entities;

import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;

import ostava.upgsp.drawables.SimpleGraphic;

/**
 * The shooter
 * 
 * @author Oto Stava A16B0148P
 */
public class Shooter extends SimpleGraphic {
	
	private static final double HEAD_RADIUS = 3;
	private static final double BODY_HEIGHT = 8;
	private static final double ARM_LENGTH = 3;
	private static final double LEGS_DOWN = 6;
	private static final double LEGS_OUT = 2;
	
	private static final double HEAD_DIAMETER = HEAD_RADIUS * 2;
	private static final double BODY_RADIUS = BODY_HEIGHT / 2;
	
	public Shooter(final double x, final double y) {
		super(x, y, Color.BLUE);
	}
	
	@Override
	public String toString() {
		return String.format("Shooter: %.2fm, %.2fm", this.x, this.y);
	}
	
	@Override
	protected void createGraphic(Path2D path) {
		path.moveTo(0, 0);
		path.append(new Ellipse2D.Double(-HEAD_RADIUS, - (BODY_RADIUS + HEAD_DIAMETER), HEAD_DIAMETER, HEAD_DIAMETER), false); // head
		path.append(new Line2D.Double(0, - BODY_RADIUS, 0, BODY_RADIUS), false); // body
		path.append(new Line2D.Double(-ARM_LENGTH, 0, ARM_LENGTH, 0), false); // arms
		path.append(new Line2D.Double(0, BODY_RADIUS, -LEGS_OUT, BODY_RADIUS + LEGS_DOWN), false); // left leg
		path.append(new Line2D.Double(0, BODY_RADIUS, LEGS_OUT, BODY_RADIUS + LEGS_DOWN), false); // right leg
	}
	
}
