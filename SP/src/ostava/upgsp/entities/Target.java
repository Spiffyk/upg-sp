package ostava.upgsp.entities;

import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.util.Random;

import ostava.upgsp.core.World;
import ostava.upgsp.drawables.SimpleGraphic;

/**
 * The target
 * 
 * @author Oto Stava A16B0148P
 */
public class Target extends SimpleGraphic {
	
	final Random r = new Random();
	
	private static final double MOVEMENT_MAX_DISTANCE_IDLE = 8;
	private static final double MOVEMENT_MAX_DISTANCE_FLEEING = 20;
	private static final double AZIMUTH_RANDOMIZE = 1;
	
	private static final double OUTER_RADIUS = 8;
	
	private static final double OUTER_DIAMETER = OUTER_RADIUS * 2;
	private static final double INNER_RADIUS = OUTER_RADIUS / 2;
	private static final double INNER_DIAMETER = INNER_RADIUS * 2;
	
	public Target(final double x, final double y) {
		super(x, y, Color.RED);
	}
	
	@Override
	public String toString() {
		return String.format("Target: %.2fm, %.2fm", this.x, this.y);
	}
	
	@Override
	protected void createGraphic(Path2D path) {
		path.append(new Ellipse2D.Double(-OUTER_RADIUS, -OUTER_RADIUS, OUTER_DIAMETER, OUTER_DIAMETER), false);
		path.append(new Ellipse2D.Double(-INNER_RADIUS, -INNER_RADIUS, INNER_DIAMETER, INNER_DIAMETER), false);
		path.append(new Line2D.Double(-OUTER_RADIUS, 0, OUTER_RADIUS, 0), false);
		path.append(new Line2D.Double(0, -OUTER_RADIUS, 0, OUTER_RADIUS), false);
	}
	
	public void update() {
		final Terrain terrain = world.getTerrain();
		final Hitspot hitspot = world.getHitspot();
		final double azimuth;
		final double distance;
		if (hitspot != null) {
			distance = r.nextDouble() * MOVEMENT_MAX_DISTANCE_FLEEING;
			final double tan = (y - hitspot.getY()) / (x - hitspot.getX());
			double azimuthAwayFromHitspot = Math.atan(tan);
			if (x <= hitspot.getX()) {
				azimuthAwayFromHitspot += Math.PI;
			}
			azimuth = azimuthAwayFromHitspot + (AZIMUTH_RANDOMIZE / 2 - r.nextDouble() * AZIMUTH_RANDOMIZE);
		} else {
			distance = r.nextDouble() * MOVEMENT_MAX_DISTANCE_IDLE;
			azimuth = r.nextDouble() * World.TAU;
		}
		
		x += Math.cos(azimuth) * distance;
		y += Math.sin(azimuth) * distance;
		
		if (x < 0) { x = 0; }
		if (x > terrain.getWidth()) { x = terrain.getWidth(); }
		if (y < 0) { y = 0; }
		if (y > terrain.getHeight()) { y = terrain.getHeight(); }
	}
}
