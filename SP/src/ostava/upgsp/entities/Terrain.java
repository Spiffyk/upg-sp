package ostava.upgsp.entities;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import ostava.upgsp.core.Point;
import ostava.upgsp.core.WorldRenderer;
import ostava.upgsp.drawables.Drawable;

public class Terrain implements Drawable {
	
	/**
	 * The image that is drawn as the visual representation of the terrain (if non-flat)
	 */
	protected final BufferedImage terrainImage;
	
	/**
	 * Terrain cells, containing altitude in metres, first coordinate being the row, second being the column
	 */
	protected final double[][] data;
	
	/**
	 * Whether the terrain is flat
	 */
	protected boolean flat;
	
	/**
	 * The altitude of the highest point in the terrain
	 */
	protected double highestAltitude;
	
	/**
	 * The altitude of the lowest point in the terrain
	 */
	protected double lowestAltitude;
	
	/**
	 * Width in m
	 */
	protected final double width;
	
	/**
	 * Height in m
	 */
	protected final double height;
	
	/**
	 * Width of a cell in m
	 */
	protected final double deltaX;
	
	/**
	 * Height of a cell in m
	 */
	protected final double deltaY;
	
	public Terrain(final int[][] terrain, final int deltaX, final int deltaY) {
		if (terrain.length < 1) {
			throw new IllegalArgumentException("Terrain has to be at least 1 high");
		}
		
		if (terrain[0].length < 1) {
			throw new IllegalArgumentException("Terrain has to be at least 1 wide");
		}
		
		// set dimensions
		this.deltaX = deltaX / 1000.0;
		this.deltaY = deltaY / 1000.0;
		this.width = terrain[0].length * this.deltaX;
		this.height = terrain.length * this.deltaY;
		
		// Convert terrain to data and collect the highest and the lowest altitude
		
		this.data = new double[terrain.length][terrain[0].length];
		
		
		for (int i = 0; i < terrain.length; i++) {
			for (int j = 0; j < terrain[i].length; j++) {
				this.data[i][j] = terrain[i][j] / 1000.0;
			}
		}
		
		// create renderable terrain image
		this.terrainImage = new BufferedImage(this.data[0].length, this.data.length, BufferedImage.TYPE_BYTE_GRAY);
		update();
	}
	
	private void update() {
		double highestAltitude = - Double.MAX_VALUE;
		double lowestAltitude = Double.MAX_VALUE;
		boolean flat = true;
		double lastMember = this.data[0][0];
		
		for (int i = 0; i < this.data.length; i++) {
			for (int j = 0; j < this.data[i].length; j++) {
				double m = this.data[i][j];
				
				if (flat && lastMember != m) {
					flat = false;
				}
				
				if (m > highestAltitude) {
					highestAltitude = m;
				}
				
				if (m < lowestAltitude) {
					lowestAltitude = m;
				}
				
				lastMember = m;
			}
		}
		
		this.highestAltitude = highestAltitude;
		this.lowestAltitude = lowestAltitude;
		this.flat = flat;
		
		byte[] imageData = ((DataBufferByte) terrainImage.getRaster().getDataBuffer()).getData();
		for (int i = 0; i < this.data.length; i++) {
			for (int j = 0; j < this.data[i].length; j++) {
				imageData[i * this.data[i].length + j] =
						(byte) (255 * (this.data[i][j] - this.lowestAltitude) / (this.highestAltitude - this.lowestAltitude));
			}
		}
	}
	
	@Override
	public void draw(Graphics2D g2, WorldRenderer r) {
		final double scale = r.getScale();
		Rectangle2D rect = new Rectangle2D.Double(0, 0, this.width * scale, this.height * scale);
		
		if (flat) {
			g2.setColor(Color.GRAY);
			g2.fill(rect);
		} else {
			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
			g2.drawImage(this.terrainImage, 0, 0, (int) (this.width * scale), (int) (this.height * scale), null);
		}
		g2.setColor(Color.BLACK);
		g2.draw(rect);
		
		Rectangle2D clip = new Rectangle2D.Double(1, 1, rect.getWidth() - 2, rect.getHeight() - 2);
		g2.setClip(clip);
	}
	
	/**
	 * Creates a crater in the terrain
	 * @param cx The X-coordinate of the centre of the crater
	 * @param cy The Y-coordinate of the centre of the crater
	 * @param depth The altitude lost in the centre
	 * @param radius The radius of the crater
	 */
	public void damage(double cx, double cy, double depth, double radius) {
		if (radius <= 0) {
			throw new IllegalArgumentException("The radius must be positive");
		}
		
		int minCol = getColumn(cx - radius);
		int maxCol = getColumn(cx + radius);
		int minRow = getRow(cy - radius);
		int maxRow = getRow(cy + radius);
		
		for (int i = minRow; i <= maxRow; i++) {
			for (int j = minCol; j <= maxCol; j++) {
				double x = getX(i);
				double y = getY(j);
				double dist = Point.distanceOf(x, y, cx, cy);
				
				if (i >= 0 && i < data.length && j >= 0 && j < data[i].length) {
					if (dist <= radius) {
						data[i][j] -= (1.0 - (dist / radius)) * depth;
					}
				}
			}
		}
		
		update();
	}
	
	public double getX(int i) {
		return i * deltaX;
	}
	
	public double getY(int j) {
		return j * deltaY;
	}
	
	/**
	 * Gets the column index from x-coordinate
	 * @param x X-coordinate in metres
	 * @return Column index
	 */
	public int getColumn(double x) {
		if (x <= 0) {
			return 0;
		}
		
		if (x > width) {
			x = width;
		}
		
		return (int)(x / deltaX);
	}
	
	/**
	 * Gets the row index from y-coordinate
	 * @param y Y-coordinate in metres
	 * @return Row index
	 */
	public int getRow(double y) {
		if (y <= 0) {
			return 0;
		}
		
		if (y > height) {
			y = height;
		}
		
		return (int) (y / deltaY);
	}
	
	/**
	 * Checks whether a point is inside the map
	 * @param x X-coordinate in m
	 * @param y Y-coordinate in m
	 * @return 
	 */
	public boolean isInside(double x, double y) {
		return (x > 0 && x < width && y > 0 && y > width);
	}
	
	/**
	 * Returns altitude in metres
	 * @param x X-coordinate in metres
	 * @param y Y-coordinate in metres
	 * @return Altitude at the point, {@code null} if outside of the map
	 */
	public Double getAltitude(double x, double y) {
		if (x >= 0 && x < width && y >= 0 && y < height) {
			return data[getRow(y)][getColumn(x)];
		} else {
			return null;
		}
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(String.format("Terrain %.2fm x %.2fm (spacing: %dmm x %dmm)\n", this.width, this.height, this.deltaX, this.deltaY));
		
		for (int y = 0; y < data.length; y++) {
			sb.append("| ");
			for (int x = 0; x < data[y].length; x++) {
				sb.append(String.format("%5dm | ", data[y][x]));
			}
			
			sb.append('\n');
		}
		
		return sb.toString();
	}

	/**
	 * @return the width in m
	 */
	public double getWidth() {
		return width;
	}

	/**
	 * @return the height in m
	 */
	public double getHeight() {
		return height;
	}
	
	/**
	 * @return {@code true} if the terrain is flat, otherwise {@code false}
	 */
	public boolean isFlat() {
		return flat;
	}

	/**
	 * @return the highestAltitude
	 */
	public double getHighestAltitude() {
		return highestAltitude;
	}

	/**
	 * @return the lowestAltitude
	 */
	public double getLowestAltitude() {
		return lowestAltitude;
	}

}
