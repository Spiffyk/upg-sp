package ostava.upgsp.util;

public class CLIMessenger implements Messenger {

	@Override
	public void error(String message, String title) {
		System.out.println("[!] " + message);
	}

	@Override
	public void info(String message, String title) {
		System.out.println("[i] " + message);
	}

}
