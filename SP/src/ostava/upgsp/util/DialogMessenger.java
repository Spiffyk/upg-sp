package ostava.upgsp.util;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class DialogMessenger implements Messenger {
	private JFrame parent = null;
	
	public void setParent(JFrame frame) {
		this.parent = frame;
	}

	@Override
	public void error(String message, String title) {
		JOptionPane.showMessageDialog(parent, message, title, JOptionPane.ERROR_MESSAGE);
	}

	@Override
	public void info(String message, String title) {
		JOptionPane.showMessageDialog(parent, message, title, JOptionPane.INFORMATION_MESSAGE);
	}

}
