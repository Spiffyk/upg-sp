package ostava.upgsp.util;

public interface Messenger {
	public void error(String message, String title);
	public default void error(String message) {
		this.error(message, "Error");
	}
	public void info(String message, String title);
	public default void info(String message) {
		this.info(message, "Info");
	}
}
