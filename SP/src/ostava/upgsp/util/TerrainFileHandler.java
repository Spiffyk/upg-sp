package ostava.upgsp.util;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import ostava.upgsp.core.World;
import ostava.upgsp.entities.Shooter;
import ostava.upgsp.entities.Target;
import ostava.upgsp.entities.Terrain;

public class TerrainFileHandler {
	
	public static World load(InputStream in, Messenger messenger) {
		DataInputStream dis = new DataInputStream(in);
		final World world = new World();
		
		try {
			final int width = dis.readInt();
			final int height = dis.readInt();
			
			if (width <= 0 || height <= 0) {
				messenger.error("Width and height have to be positive", "File error");
				System.exit(-2);
			}
			
			final int deltaX = dis.readInt();
			final int deltaY = dis.readInt();
			
			if (deltaX <= 0 || deltaY <= 0) {
				messenger.error("Deltas have to be positive", "File error");
				System.exit(-1);
			}
			
			final int[][] terrainData = new int[height][width];
			
			final double shooterX = dis.readInt() * deltaX / 1000.0;
			final double shooterY = dis.readInt() * deltaY / 1000.0;
			final Shooter shooter = new Shooter(shooterX, shooterY);
			world.setShooter(shooter);
			
			final double targetX = dis.readInt() * deltaX / 1000.0;
			final double targetY = dis.readInt() * deltaY / 1000.0;
			final Target target = new Target(targetX, targetY);
			world.setTarget(target);
			
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					terrainData[y][x] = dis.readInt();
				}
			}
			
			final Terrain terrain = new Terrain(terrainData, deltaX, deltaY);
			world.setTerrain(terrain);
		} catch (IOException e) {
			messenger.error("There was an error reading the file", "File error");
			System.err.println(e.getStackTrace());
			System.exit(1);
		}
		
		return world;
	}
	
	public static World load(File file, Messenger messenger) throws FileNotFoundException {
		return load(new FileInputStream(file), messenger);
	}
	
	public static World load(String filename, Messenger messenger) throws FileNotFoundException {
		return load(new File(filename), messenger);
	}
}
